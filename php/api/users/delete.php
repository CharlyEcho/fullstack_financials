<?php
    // HEADER FOR HTTP REQUEST
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET, PUT');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    // INLUDING THE CREATED CLASSES
    include_once '../../config/Database.php';
    include_once '../../models/User.php';

    // DATABASE: INSTANCIATING AND CONNECTION
    $database = new Database();
    $dbconn = $database->connect();

    // INSTANCIATING
    $user = new User($dbconn);

    // GETTING USER NAME WITH GET-VARIABLE
    $user->u_id = (isset($_REQUEST['u_id']) && $_REQUEST['u_id'] !=="" ) ? $_REQUEST['u_id'] : die();
    
    /*
    // GETTING USER NAME AS VIA FROM AND JSON
    $data = json_decode(file_get_contents("php://input"));
    $user->u_id = $data->u_id;
    */

    // USER: READING FROM DATABASE
    if($user->delete())
    {
        echo json_encode(
            array("message" => "User $user->u_name deleted")
        );
    }
    else
    {
        echo json_encode(
            array("message" => "User $user->u_name NOT deleted")
        );
    };