<?php
    // HEADER FOR HTTP REQUEST
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    // INLUDING THE CREATED CLASSES
    include_once '../../config/Database.php';
    include_once '../../models/User.php';

    // DATABASE: INSTANCIATING AND CONNECTION
    $database = new Database();
    $dbconn = $database->connect();

    // USER: INSTANCIATING
    $user = new User($dbconn);

    // USER: GETTING ID
    //$user->u_id = isset($_GET['u_id']) ? $_GET['u_id'] : die();
    $user->u_id = isset($_REQUEST['u_id']) ? $_REQUEST['u_id'] : die();
    //echo($user->u_id);

    // USER: READING FROM DATABASE
    $result = $user->read_single();

    // HANDLE THE RESULT
    $resultCount = $result->rowCount();

    if($resultCount > 0)
    {
        $user_array = array();
        $user_array['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC))
        {
            // MAKE VARIABLES USEABLE
            extract($row);

            // USE VARIABLES BY CREATING ITEM-ARRAY
            $user_item = array(
                'u_id' => $u_id,
                'u_name' => $u_name
            );
            
            // APPEND ITEM TO ARRAY
            array_push($user_array['data'], $user_item);
        }
        //CONVERT ARRAY TO JSON
        echo json_encode($user_array);
    }
    else
    {
        echo json_encode(array('message' => 'no user found'));
    }
      
        
        