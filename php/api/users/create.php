<?php
    // HEADER FOR HTTP REQUEST
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET, POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    // INLUDING THE CREATED CLASSES
    include_once '../../config/Database.php';
    include_once '../../models/User.php';

    // DATABASE: INSTANCIATING AND CONNECTION
    $database = new Database();
    $dbconn = $database->connect();

    // INSTANCIATING
    $user = new User($dbconn);

    // GETTING USER NAME WITH GET-VARIABLE
    $user->u_name = (isset($_REQUEST['u_name']) && $_REQUEST['u_name'] !=="" ) ? $_REQUEST['u_name'] : die();
    //$user->u_name = isset($_GET['u_name']) ? $_GET['u_name'] : die();
   
    /*
    // GETTING USER NAME AS VIA FROM AND JSON
    $data = json_decode(file_get_contents("php://input"));
    $user->u_name = $data->u_name;
    */

    // USER: READING FROM DATABASE
    if($user->create())
    {
        echo json_encode(
            array("message" => "User $user->u_name created")
        );
    }
    else
    {
        echo json_encode(
            array("message" => "User $user->u_name NOT created")
        );
    };    
        
        