<?php

    class User
    {

        // GENERAL USER PROPERTIES
        private $conn;
        private $table = 'users';

        // SPECIFIC USER PROPERTIES
        public $u_id;
        public $u_name;

        // CONSTRUCTOR
        public function __construct($db)
        {
            $this->conn = $db;
        }


        /* READING ALL USERS */

        public function read_all()
        {
            // CREATING THE STATEMENT
            $sql = "SELECT * FROM " . $this->table . ";";
                    
            // PREPARATION OF STATEMENT
            $stmt = $this->conn->prepare($sql);
           
            // EXECUTION OF STATEMENT
            $stmt->execute();

            return $stmt;
        }


        /* READING A SINGLE USER */

        public function read_single()
        {
            // CREATING THE STATEMENT
            $sql = "SELECT u_id, u_name FROM $this->table WHERE u_id = $this->u_id;";
        
            // PREPARATION OF STATEMENT
            $stmt = $this->conn->prepare($sql);

            // EXECUTION OF STATEMENT
            $stmt->execute();

            return $stmt;
        }

        /* CREATING NEW USER */

        public function create()
        {
            // CLEANING UP THE INSERTION
            $this->u_name = htmlspecialchars(strip_tags($this->u_name));

            // CREATING THE STATEMENT
            $sql = "INSERT INTO $this->table (u_name) VALUES ('$this->u_name');";

            // PREPARING THE STATEMENT
            $stmt = $this->conn->prepare($sql);
            
            // EXECUTING THE STATEMENT
            return ($stmt->execute() ? true : false); 
        }

        /* UPDATING EXISTING USER */

        public function update()
        {
            // CLEANING UP THE INSERTION
            $this->u_id = htmlspecialchars(strip_tags($this->u_id));
            $this->u_name = htmlspecialchars(strip_tags($this->u_name));

            // CREATING THE STATEMENT
            $sql = "UPDATE $this->table SET u_name = '$this->u_name' WHERE u_id = $this->u_id;";

            // PREPARING THE STATEMENT
            $stmt = $this->conn->prepare($sql);
            
            // EXECUTING THE STATEMENT
            return ($stmt->execute() ? true : false); 

        }

        /* DELETING USER */

        public function delete()
        {
            // CREATING THE STATEMENT
            $sql = "DELETE FROM $this->table WHERE u_id = $this->u_id;";
        
            // PREPARATION OF STATEMENT
            $stmt = $this->conn->prepare($sql);

            // EXECUTION OF STATEMENT
            return ($stmt->execute() ? true : false); 
        }
    }
