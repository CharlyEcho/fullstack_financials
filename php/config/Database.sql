-- RE/CREATING DATABASE
--DROP DATABASE IF EXISTS finances;
--CREATE DATABASE finances;
--connect finances;

-- DELETING TABLES
DROP TABLE IF EXISTS
	transactions,
	users,
	categories;

-- CREATING TABLES
CREATE TABLE users(
	u_id serial PRIMARY KEY,
	u_name varchar NOT NULL
);
CREATE TABLE categories(
	c_id serial PRIMARY KEY,
	c_name varchar NOT NULL
);
CREATE TABLE transactions(
	t_id serial PRIMARY KEY,
	t_booking_date DATE NOT NULL DEFAULT CURRENT_DATE,
	t_transaction_date DATE NOT NULL,	
	t_categorie integer REFERENCES users (u_id),
	t_user integer REFERENCES categories (c_id)
);

-- INSERTING CONTENT
INSERT INTO users(u_name) VALUES('ce'), ('ms');
INSERT INTO categories(c_name) VALUES('Einzahlung'), ('Lebensmittel'), ('Restaurant');