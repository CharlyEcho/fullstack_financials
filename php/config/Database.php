<?php

    class Database
    {
        private $dbdriver = "pgsql";
        private $host = 'localhost';
        private $port = '5432';
        private $dbname = 'finances';
        private $user = 'postgres';
        private $password = 'postgres';
        private $conn;

        public function connect()
        {
            $this->conn = null;

            try
            {
                $dsn = "$this->dbdriver:dbname=$this->dbname;host=$this->host";
                $this->conn = new PDO($dsn, $this->user, $this->password);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            } catch (PDOException $e) 
            {
                echo 'Connection Error: ' . $e->getMessage();
            }
            return $this->conn;
        }
    }