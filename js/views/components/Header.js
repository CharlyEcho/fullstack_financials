let Header = {
    render: async () => {
        let view =  /*html*/`
        <header class="header">
            <nav class="navbar navbar-expand-md navbar-light ">
                <a href="#home" class="navbar-brand">FINANCES</a>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link" href="#user">Users</a>
                        <a class="nav-item nav-link" href="#transaction">Transaktionen</a>
                        <a class="nav-item nav-link" href="#category">Kategorien</a>
                    </div>
                    <div class="navbar-nav ml-auto">
                        <a href="#" class="nav-item nav-link">Login</a>
                    </div>
                </div>
            </nav>
        </header>
        `
        return view
    },
    after_render: async () => {

        
    }

}

export default Header;