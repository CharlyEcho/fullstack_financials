let Homepage = {
    render : async () =>
    {
        let view =  /*html*/`
            <section class="section text-center">

                <h1 class="display-1 mt-4 text-center">Finances</h1>

                <h3 class="display-4 mt-4 text-center">This is the beginning of my new project.</h3>

                <ul class="mt-4 list-group" >
                    <li class="list-group-item">full stack projekt</li>
                    <li class="list-group-item">Single Page Application (SPA)</li>
                    <li class="list-group-item">tracking and analyzing cash flow</li>
                    <li class="list-group-item">frontside: html, css/bootstrap, plain js ES6</li>
                    <li class="list-group-item" >backside: php, postgresql</li>
                    <li class="list-group-item" >connected by ReST API</li>
                </ul>

            </section>
        `
        return view
    },
    after_render: async () =>
    {

    }

}

export default Homepage;