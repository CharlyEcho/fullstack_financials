import User_Storage     from "../../services/User_Storage.js";
import User_Interface   from "../../services/User_Interface.js"

class Userpage {

    //  P R O P E R T I E S
    constructor()
    {
        this.userlist = []
        this.user = {u_id: null, u_name: null}
    }

    //  R E N D E R I N G
    static async render ()
    { 
        return User_Interface.renderSection();
    };

    //  A F T E R   R E N D E R I N G
    static async after_render()
    {
        User_Interface.renderModal()
        this.readUsers();
        this.handleEvents();
    }


    /*  C R U D   M E T H O D S */ 

    //  C R E A T E
    static async createUser()
    {
        // GET FORM VALUES
        const user_id = 0 || this.userlist[this.userlist.length-1].u_id+1
        const user_name = document.querySelector('#user_name').value
        
        // VALIDATE VALUES
        if(user_name === '')
        {
            User_Interface.showAlert('Pleasy fill in user name', 'danger')
        }
        else
        {
            // UI AND STORAGE
            this.user = {u_id: user_id, u_name : user_name}
            User_Interface.createUser(this.user)
            User_Storage.createUser(this.user)
            User_Interface.resetForm()
            User_Interface.showAlert(`User ${this.user.u_name} created`, 'success')
            
            // ARRAY
            this.userlist.push(this.user)
        }
    }

    //  R E A D   A L L
    static async readUsers()
    {
        User_Interface.emptyTableBody()
        User_Storage.readUsers().then(userlist =>
            {
                // ARRAY
                this.userlist = userlist
                
                // UI (STORAGE NOT INVOLVED BECAUSE OF PERFORMANCE)
                User_Interface.fillTableBody(this.userlist)
            });
    }

    //  R E A D   S I N G L E
    static async readUser(target)
    {
        if(target.classList.contains('warning'))
        {
            let u_id = target.parentElement.parentElement.childNodes[1].innerHTML;
            
            // UI AND STORAGE
            User_Storage.readUser(u_id).then(users =>
            {
                users.forEach(user => {
                    User_Interface.addUserToForm(user)
                    User_Interface.showAlert(`User ${user.u_name} ready for update`, 'warning')
                })   
            })

            // ARRAY
            // this.userlist.filter(user => user.u_id == `${u_id}`)[0]
        }    
    }

    //  U P D A T E
    static async updateUser()
    {
        // GET FORM VALUES
        const user_id = document.querySelector('#user_id').value
        const user_name = document.querySelector('#user_name').value
        
        // VALIDATE VALUES
        if(user_name === '')
        {
            User_Interface.showAlert('Pleasy fill in user name', 'danger')
        }
        else
        {
            this.user = {u_id: user_id, u_name : user_name}
            
            // UI AND STORAGE
            User_Storage.updateUser(this.user)
            User_Interface.updateUser(this.user)
            User_Interface.resetForm()
            User_Interface.showAlert(`User ${this.user.u_name} updated`, 'success')
            
            // ARRAY
            let user_filtered = this.userlist.filter(u => u.u_id == `${this.user.u_id}`)[0]
            let user_index = this.userlist.indexOf(user_filtered)
            this.userlist[user_index] = {u_id: this.user.u_id, u_name : this.user.u_name}
        }
    }

    //  D E L E T E
    static async deleteUser(target)
    {
        if(target.classList.contains('delete'))
        {
            // UI AND STORAGE
            let u_id = target.parentElement.parentElement.childNodes[1].innerHTML;
            User_Interface.deleteUser(u_id)
            User_Storage.deleteUser(u_id);

            // ARRAY
            this.user = this.userlist.filter(u => u.u_id == `${u_id}`)
            this.userlist.splice(this.userlist.indexOf(this.userlist.filter(user => user.u_id === `${u_id}`)), 1)
        }    
    }


    /*  E V E N T   H A N D L I N G */ 
    static async handleEvents()
    {

        //  C L I C K   T A B L E
        document.querySelector("#user_table_body").addEventListener('click', (e) =>
        {
            //  R E A D
            if (e.target.classList.contains('warning'))
            {
                this.readUser(e.target);
                User_Interface.setForm();   
                document.querySelector('#user_form_submit').value = "Update User"
            }

            //  D E L E T E
            else if (e.target.classList.contains('delete'))
            {
                let target = e.target
                
                //  C L I C K   M O D A L
                $('#user_modal').on('shown.bs.modal', (e) =>
                {
                    //  C O N T I N U E
                    document.querySelector('#user_modal_button_continue').addEventListener('click', (e) => 
                    {
                        this.deleteUser(target);
                        User_Interface.showAlert('User deleted', 'success')                        
                    })
                })
                
            }
        });

        //  S U B M I T
        document.querySelector(`#user_form`).addEventListener('submit', (e) =>
        {
            e.preventDefault()

            //  C R E A T E
            if(e.target.querySelector('#user_form_submit').value.toLowerCase().includes("add"))
            {
                this.createUser();
            }

            //  U P D A T E
            else if (e.target.querySelector('#user_form_submit').value.toLowerCase().includes("update"))
            {
                this.updateUser();
            }
        })    
    }
}

export default Userpage;