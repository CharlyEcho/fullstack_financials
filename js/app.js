"use strict";
    
// IMPORT PAGES
import Homepage from "./views/pages/Homepage.js";
import Userpage from "./views/pages/Userpage.js";
import Error404 from "./views/pages/Error404.js";

// IMPORT COMPONENTS
import Footer   from "./views/components/Footer.js"; 
import Header   from "./views/components/Header.js";

// IMPORT SERVICES
import Utilities    from "./services/Utilities.js";

class App
{
    init()
    {
        this.render()
        this.route()
    }
    render()
    {
        // GET OUTER CONTAINER ELEMENT
        let app = document.querySelector("#app")

        // DEFINE INNER CONTAINER ELEMENTS
        let container = [
            {
                type    :   "div",
                id      :   "header_container",
                class   :   "container mt-4 text-center"
            },
            {   type    :   "div",
                id      :   "main_container",
                class   :   "container mt-4 text-center"
            },
            {
                type    :   "div",
                id      :   "footer_container",
                class   :   "container mt-4 text-center"
            }]

        // CREATE INNER CONTAINER ELEMENTS
        let fragment = document.createDocumentFragment()
        container.forEach(el => {
            let newElement = document.createElement(`${el.type}`)
            newElement.id = el.id
            newElement.class = el.class
            fragment.appendChild(newElement)
        })
        app.appendChild(fragment)
    }
    route()
    {
        // LIST OF SUPPORTED ROUTES
        const routes = {
            "/"             :   Homepage,
            "/home"         :   Homepage,
            "/user"         :   Userpage
        };
        
        // THE ROUTER
        const router = async () =>
        {
            // SELECT ELEMENTS
            const   header  = null || document.querySelector("#header_container");
            const   main    = null || document.querySelector("#main_container");
            const   footer  = null || document.querySelector("#footer_container");
        
            // RENDER COMPONENTS
            header.innerHTML = await Header.render();
            await Header.after_render();
            footer.innerHTML = await Footer.render();
            await Footer.after_render();
        
            // CHECK AND PARSE REQUEST
            let request = Utilities.parseRequestURL();
            let parsedURL = (request.resource ? '/' + request.resource : '/') + (request.id ? '/:id' : '') + (request.verb ? '/' + request.verb : '');
        
            // GET PAGE FROM ROUTER BY REQUEST
            let page = routes[parsedURL] ? routes[parsedURL] : Error404;
            main.innerHTML = await page.render();
            await page.after_render();
        }
    
        // EVENT LISTENER
        window.addEventListener("hashchange", router);
        window.addEventListener("load", router);  
    }
}

let app = new App()
app.init()