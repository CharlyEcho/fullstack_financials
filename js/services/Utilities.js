const Utilities = {

    //  P A R S E  A N D   B R E A K   U R L
    parseRequestURL : () =>
    {
        let url = location.hash.slice(1).toLocaleLowerCase() || '/';
        let r = url.split("/");
        let request = {
            resource    :   null,
            id          :   null,
            verb        :   null
        }
        request.resource    = r[0];
        request.id          = r[1];
        request.verb        = r[2];

        return request;
    },

    sleep : (ms) =>
    {
        return new Promise(resolve => setTimeout(resolve, ms));
    },

    //  S E N D   A J A X   R E Q U E S T   T O   R E S T - A P I
    request : (method, url, data = null) =>
    {
        return new Promise((resolve, reject) =>
        {
            let ajax = new XMLHttpRequest()
            ajax.open(method, url, true)
            ajax.setRequestHeader('Content-Type', 'application/json')
            ajax.onload = () =>
                {
                    if (ajax.status == 200)
                    {
                        return resolve(JSON.parse(ajax.responseText || '{}').data)
                    }
                    else
                    {
                        return reject(new Error(`Request failed with status ${ajax.status}`))
                    }
                }
            data == null ? ajax.send() : ajax.send(JSON.stringify(data))
        })

    }
}

export default Utilities;