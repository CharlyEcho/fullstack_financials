// CLASS FOR DATA INTERFACE HANDLING (MODEL)
class User_Interface
{

    // REMOVE ALL TABLE ROWS
    static async emptyTableBody()
    {
        let user_table_body = document.querySelector('#user_table_body');
        while (user_table_body.firstChild)
        {
            user_table_body.removeChild(user_table_body.firstChild)
        }
    }

    // CREATE ALL TABLE ROWS
    static async fillTableBody(userlist=[])
    {
        Array.from(userlist).forEach(user => this.createTableRow(user));       
    }

    // ADD THE USER TO LIST AS ROW
    static async createTableRow(user)
    {
        // SELECT WRAPPER ELEMENTS
        let user_table_body = document.querySelector('#user_table_body');

        // CREATE FRAGMENT AS TEMPORARY CONTAINER
        let fragment = document.createDocumentFragment()

        // CREATE SPECIFIC LIST ELEMENT
        let user_row = document.createElement('tr');
        user_row.id = `user_table_row_id_${user.u_id}`
        
        // INSERT INFORMATION IN THE HTML TAG
        user_row.innerHTML = `
            <th class="d-none" scope="row">${user.u_id}</th>
            <td id="user_table_row_id_${user.u_id}_value" class="text-center">${user.u_name}</td>
            <td class="text-center">
                <a href="#user" class="btn btn-secondary btn-sm warning">O</a>
                <a href="#user" class="btn btn-light btn-sm delete" data-toggle="modal" data-target="#modal_delete">X</a>
            </td>
            `;
        
        fragment.appendChild(user_row);
        user_table_body.appendChild(fragment);     
    }

    // CREATE ROW
    static async createUser(user)
    {
        this.createTableRow(user)
    }

    //  U P D A T E   R O W
    static async updateUser(user)
    {
        let user_table_row = document.querySelector(`#user_table_row_id_${user.u_id}`);
        let user_table_row_value = document.querySelector(`#user_table_row_id_${user.u_id}_value`);
        user_table_row_value.innerHTML = user.u_name
    }

    //  D E L E T E   R O W
    static async deleteUser(u_id)
    {
        let user_table_row = document.querySelector(`#user_table_row_id_${u_id}`);
        user_table_row.remove();
    }

    //  F I L L   U S E R   D A T A   I N T O   F O R M
    static async addUserToForm(user)
    {
        document.querySelector('#user_id').value = user.u_id;
        document.querySelector('#user_name').value = user.u_name;
    }

    //  S E T   F O R M   E L E M E N T S   F O R   U P D A T E
    static async setForm()
    {
        let user_form_abort = document.querySelector("#user_form_abort")
        user_form_abort.className = "btn btn-light btn-block"
        user_form_abort.addEventListener('click', (e) => 
        {
            e.preventDefault()
            User_Interface.resetForm()
        }) 
        document.querySelector('#user_form_submit').after(user_form_abort)
    }

    //  R E S E T   F O R M
    static async resetForm()
    {
        document.querySelector("#user_form").reset()
        document.querySelector('#user_form_submit').value = "Add User"     
        document.querySelector('#user_form_abort').className = 'btn btn-secondary btn-block d-none'   
    }


    //  S T A T U S   I N F O R M A T I O N
    static async showAlert(message, className)
    {
        // CREATE MESSAGE CONTAINER
        const message_div = document.createElement('div')
        message_div.className = `alert alert-${className} mt-4`

        // INSERT MESSAGE INTO CREATED CONTAINER
        message_div.appendChild(document.createTextNode(message))

        // INSERT CREATED CONTAINER INTO EXISTING HTML STRUCTURE
        const user_container = document.querySelector('#user_container')
        const user_form = document.querySelector('#user_form')
        //user_container.insertBefore(message_div, user_form)
        //message_div.after(user_form)
        user_form.after(message_div)

        // REMOVE MESSAGE AFTER A FEW SECONDS FROM STRUCTURE
        setTimeout(() => document.querySelector('.alert').remove(), 3000)
    }

    //  G E N E R A L   P A G E   S E C T I O N
    static async renderSection()
    {
        let section =  /*html*/`
        <section class="section">
            <h1 class="display-4 mt-4 text-center"> Userpage </h1>
            <div id="user_container" class="container mt-4 text-center">
                <table id="user_table" class="table table-hover text-center">
                    <thead id="user_table_head">
                        <th class="d-none" scope="col">#</th>
                        <th scope="col" >Username</th>
                        <th scope="col">Action</th>
                    </thead>
                    <tbody id="user_table_body">
                    </tbody>
                </table>   
                <form id="user_form">
                    <div class="form-group d-none">
                        <label for="user_id">UserID</label>
                        <input type="text" id='user_id' class='form-control'>
                    </div>
                    <div class="form-group">
                        <label for="user_name">Username</label>
                        <input type="text" id='user_name' class='form-control'>
                    </div>
                    <input type="submit" id="user_form_submit" value='Add User' class='btn btn-secondary btn-block'>
                    <input type="button" id="user_form_abort" value='Abort' class='btn btn-secondary btn-block d-none'>
                </form>        
            </div>
        </section>
    `
    return section

    }
    
    //  S E C U R I T Y   R E Q U E S T
    static async renderModal()
    {
        let view =  /*html*/`
        <div class="modal" id="modal_delete" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">

                <!--Content-->
                <div class="modal-content">
                
                    <!--Head-->
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <!--Body-->
                    <div class="modal-body">
                        <div class="row d-flex justify-content-center align-items-center">
                        <p class="pt-3 pr-2">Are you sure, that you want to continue?</p>
                    </div>
                    
                    <!--Footer-->
                    <div>
                        <button id="user_modal_button_continue" type="button" class="btn btn-secondary" data-dismiss="modal">Continue</button>
                        <button id="user_modal_button_abort" type="button" class="btn btn-light" data-dismiss="modal">Abort</button>
                    </div>

                </div>
                <!--/.Content-->
            </div>
        </div>
        `
        let user_modal = document.createElement("div")
        user_modal.id = "user_modal"
        user_modal.className = "container"
        user_modal.innerHTML = view
        document.querySelector("#user_container").appendChild(user_modal)
    }
}

export default User_Interface