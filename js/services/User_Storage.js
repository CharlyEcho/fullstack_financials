import Utilities from "./Utilities.js"

// CLASS FOR DATA USER STORAGE HANDLING (MODEL)
class User_Storage
{
    /* READING ALL USERS */
    static async readUsers()
    {
        try
        {
            return await Utilities.request("GET", "./php/api/users/read_all.php")
        }
        catch (err)
        {
            console.log(err)
        }
    };

    // static async readUsers()
    // {
    //     return await fetch('./php/api/users/read_all.php')
    //         .then(response => {
    //             return response.json();
    //         })
    //         .then(json => {
    //             // console.log(json);
    //             return json.data
    //         })
    //         .catch((error) => {
    //             console.error('Error:', error);
    //         });
    // };

    /* READING SINGLE USER*/
    static async readUser(u_id)
    {
        return await fetch(`./php/api/users/read_single.php?u_id=${u_id}`)
            .then(response => {
                return response.json();
            })
            .then(json => {
                // console.log(json);
                return json.data
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    /* CREATING SINGLE USER*/
    static createUser(user)
    {
        fetch(`./php/api/users/create.php?u_name=${user.u_name}`)
        .then(response => {
            return response.json();
        })
        .then(data => {
            // console.log(data);
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }

    /* UPDATING USER*/
    static updateUser(user)
    {
        fetch(`./php/api/users/update.php?u_id=${user.u_id}&u_name=${user.u_name}`)
        .then(response => {
            return response.json();
        })
        .then(data => {
            // console.log(data);
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }

    /* DELETING SINGLE USER*/
    static deleteUser(u_id)
    {
        fetch(`./php/api/users/delete.php?u_id=${u_id}`)
        .then(response => {
            return response.json();
        })
        .then(data => {
            // console.log(data);
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }
}

export default User_Storage;